/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cstlm
 */
@Entity
@Table(name = "APPLICANT", uniqueConstraints =
{
   @UniqueConstraint(columnNames =
   {
       "CV_FILE_NAME"
   })
})
@NamedQueries(
        {
            @NamedQuery(name = "Applicant.findAll", query = "SELECT a FROM Applicant a"),
            @NamedQuery(name = "Applicant.findById", query = "SELECT a FROM Applicant a WHERE a.id = :id"),
            @NamedQuery(name = "Applicant.findByName", query = "SELECT a FROM Applicant a WHERE a.name = :name"),
            @NamedQuery(name = "Applicant.findByCvFileName", query = "SELECT a FROM Applicant a WHERE a.cvFileName = :cvFileName"),
            @NamedQuery(name = "Applicant.findByInterviewDate", query = "SELECT a FROM Applicant a WHERE a.interviewDate = :interviewDate")
        })
public class Applicant implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 55)
    @Column(name = "NAME", nullable = false, length = 55)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "CV_FILE_NAME", nullable = false, length = 255)
    private String cvFileName;
    @Column(name = "INTERVIEW_DATE")
    @Temporal(TemporalType.DATE)
    private Date interviewDate;
    @JoinColumn(name = "JOB_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Job job;

    public Applicant()
    {
    }

    public Applicant(Integer id)
    {
        this.id = id;
    }

    public Applicant(Integer id, String name, String cvFileName)
    {
        this.id = id;
        this.name = name;
        this.cvFileName = cvFileName;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCvFileName()
    {
        return cvFileName;
    }

    public void setCvFileName(String cvFileName)
    {
        this.cvFileName = cvFileName;
    }

    public Date getInterviewDate()
    {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate)
    {
        this.interviewDate = interviewDate;
    }

    public Job getJob()
    {
        return job;
    }

    public void setJob(Job job)
    {
        this.job = job;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Applicant))
        {
            return false;
        }
        Applicant other = (Applicant) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString()
    {
        return "entityBeans.Applicant[ id=" + id + " ]";
    }

}
