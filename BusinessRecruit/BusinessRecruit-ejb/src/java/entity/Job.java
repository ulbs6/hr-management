/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cstlm
 */
@Entity
@Table(name = "JOB")
@NamedQueries(
        {
            @NamedQuery(name = "Job.findAll", query = "SELECT j FROM Job j"),
            @NamedQuery(name = "Job.findById", query = "SELECT j FROM Job j WHERE j.id = :id"),
            @NamedQuery(name = "Job.findByName", query = "SELECT j FROM Job j WHERE j.name = :name"),
            @NamedQuery(name = "Job.findByRequiredEmployees", query = "SELECT j FROM Job j WHERE j.requiredEmployees = :requiredEmployees"),
            @NamedQuery(name = "Job.findByApproved", query = "SELECT j FROM Job j WHERE j.approved = :approved"),
            @NamedQuery(name = "Job.findByClosed", query = "SELECT j FROM Job j WHERE j.closed = :closed"),
            @NamedQuery(name = "Job.findAllApproved", query = "SELECT j FROM Job j WHERE j.approved = 1")
        })
public class Job implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 55)
    @Column(name = "NAME", nullable = false, length = 55)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 32700)
    @Column(name = "BENEFITS", nullable = false, length = 32700)
    private String benefits;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 32700)
    @Column(name = "REQUIREMENTS", nullable = false, length = 32700)
    private String requirements;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQUIRED_EMPLOYEES", nullable = false)
    private int requiredEmployees;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPROVED", nullable = false)
    private Boolean approved;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOSED", nullable = false)
    private Boolean closed;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "job")
    private Collection<Applicant> applicantCollection;

    public Job()
    {
    }

    public Job(Integer id)
    {
        this.id = id;
    }

    public Job(Integer id, String name, String benefits, String requirements, int requiredEmployees, Boolean approved, Boolean closed)
    {
        this.id = id;
        this.name = name;
        this.benefits = benefits;
        this.requirements = requirements;
        this.requiredEmployees = requiredEmployees;
        this.approved = approved;
        this.closed = closed;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBenefits()
    {
        return benefits;
    }

    public void setBenefits(String benefits)
    {
        this.benefits = benefits;
    }

    public String getRequirements()
    {
        return requirements;
    }

    public void setRequirements(String requirements)
    {
        this.requirements = requirements;
    }

    public int getRequiredEmployees()
    {
        return requiredEmployees;
    }

    public void setRequiredEmployees(int requiredEmployees)
    {
        this.requiredEmployees = requiredEmployees;
    }

    public Boolean getApproved()
    {
        return approved;
    }

    public void setApproved(Boolean approved)
    {
        this.approved = approved;
    }

    public Boolean getClosed()
    {
        return closed;
    }

    public void setClosed(Boolean closed)
    {
        this.closed = closed;
    }

    public Collection<Applicant> getApplicantCollection()
    {
        return applicantCollection;
    }

    public void setApplicantCollection(Collection<Applicant> applicantCollection)
    {
        this.applicantCollection = applicantCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Job))
        {
            return false;
        }
        Job other = (Job) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "entityBeans.Job[ id=" + id + " ]";
    }

}
