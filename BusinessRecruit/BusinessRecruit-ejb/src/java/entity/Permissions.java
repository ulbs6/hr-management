/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cstlm
 */
@Entity
@Table(name = "PERMISSIONS")
@NamedQueries(
{
    @NamedQuery(name = "Permissions.findAll", query = "SELECT p FROM Permissions p"),
    @NamedQuery(name = "Permissions.findById", query = "SELECT p FROM Permissions p WHERE p.id = :id"),
    @NamedQuery(name = "Permissions.findByAddNewUser", query = "SELECT p FROM Permissions p WHERE p.addNewUser = :addNewUser"),
    @NamedQuery(name = "Permissions.findByEditUser", query = "SELECT p FROM Permissions p WHERE p.editUser = :editUser"),
    @NamedQuery(name = "Permissions.findByDeleteUser", query = "SELECT p FROM Permissions p WHERE p.deleteUser = :deleteUser"),
    @NamedQuery(name = "Permissions.findByAddNewJob", query = "SELECT p FROM Permissions p WHERE p.addNewJob = :addNewJob"),
    @NamedQuery(name = "Permissions.findByEditJob", query = "SELECT p FROM Permissions p WHERE p.editJob = :editJob"),
    @NamedQuery(name = "Permissions.findByCloseJob", query = "SELECT p FROM Permissions p WHERE p.closeJob = :closeJob"),
    @NamedQuery(name = "Permissions.findByAddApplicant", query = "SELECT p FROM Permissions p WHERE p.addApplicant = :addApplicant"),
    @NamedQuery(name = "Permissions.findByEditAplicant", query = "SELECT p FROM Permissions p WHERE p.editAplicant = :editAplicant"),
    @NamedQuery(name = "Permissions.findByRemoveApplicant", query = "SELECT p FROM Permissions p WHERE p.removeApplicant = :removeApplicant")
})
public class Permissions implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADD_NEW_USER", nullable = false)
    private Boolean addNewUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDIT_USER", nullable = false)
    private Boolean editUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DELETE_USER", nullable = false)
    private Boolean deleteUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADD_NEW_JOB", nullable = false)
    private Boolean addNewJob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDIT_JOB", nullable = false)
    private Boolean editJob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOSE_JOB", nullable = false)
    private Boolean closeJob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADD_APPLICANT", nullable = false)
    private Boolean addApplicant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDIT_APLICANT", nullable = false)
    private Boolean editAplicant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REMOVE_APPLICANT", nullable = false)
    private Boolean removeApplicant;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permissions")
    private Collection<Role> roleCollection;

    public Permissions()
    {
    }

    public Permissions(Integer id)
    {
        this.id = id;
    }

    public Permissions(Integer id, Boolean addNewUser, Boolean editUser, Boolean deleteUser, Boolean addNewJob, Boolean editJob, Boolean closeJob, Boolean addApplicant, Boolean editAplicant, Boolean removeApplicant)
    {
        this.id = id;
        this.addNewUser = addNewUser;
        this.editUser = editUser;
        this.deleteUser = deleteUser;
        this.addNewJob = addNewJob;
        this.editJob = editJob;
        this.closeJob = closeJob;
        this.addApplicant = addApplicant;
        this.editAplicant = editAplicant;
        this.removeApplicant = removeApplicant;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Boolean getAddNewUser()
    {
        return addNewUser;
    }

    public void setAddNewUser(Boolean addNewUser)
    {
        this.addNewUser = addNewUser;
    }

    public Boolean getEditUser()
    {
        return editUser;
    }

    public void setEditUser(Boolean editUser)
    {
        this.editUser = editUser;
    }

    public Boolean getDeleteUser()
    {
        return deleteUser;
    }

    public void setDeleteUser(Boolean deleteUser)
    {
        this.deleteUser = deleteUser;
    }

    public Boolean getAddNewJob()
    {
        return addNewJob;
    }

    public void setAddNewJob(Boolean addNewJob)
    {
        this.addNewJob = addNewJob;
    }

    public Boolean getEditJob()
    {
        return editJob;
    }

    public void setEditJob(Boolean editJob)
    {
        this.editJob = editJob;
    }

    public Boolean getCloseJob()
    {
        return closeJob;
    }

    public void setCloseJob(Boolean closeJob)
    {
        this.closeJob = closeJob;
    }

    public Boolean getAddApplicant()
    {
        return addApplicant;
    }

    public void setAddApplicant(Boolean addApplicant)
    {
        this.addApplicant = addApplicant;
    }

    public Boolean getEditAplicant()
    {
        return editAplicant;
    }

    public void setEditAplicant(Boolean editAplicant)
    {
        this.editAplicant = editAplicant;
    }

    public Boolean getRemoveApplicant()
    {
        return removeApplicant;
    }

    public void setRemoveApplicant(Boolean removeApplicant)
    {
        this.removeApplicant = removeApplicant;
    }

    public Collection<Role> getRoleCollection()
    {
        return roleCollection;
    }

    public void setRoleCollection(Collection<Role> roleCollection)
    {
        this.roleCollection = roleCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permissions))
        {
            return false;
        }
        Permissions other = (Permissions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "entityBeans.Permissions[ id=" + id + " ]";
    }

}
