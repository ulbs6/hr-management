/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cstlm
 */
@Entity
@Table(name = "ROLE", uniqueConstraints =
{
   @UniqueConstraint(columnNames =
   {
       "NAME"
   })
})
@NamedQueries(
        {
            @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
            @NamedQuery(name = "Role.findById", query = "SELECT r FROM Role r WHERE r.id = :id"),
            @NamedQuery(name = "Role.findByName", query = "SELECT r FROM Role r WHERE r.name = :name")
        })
public class Role implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 55)
    @Column(name = "NAME", nullable = false, length = 55)
    private String name;
    @JoinColumn(name = "PERMISSIONS_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    private Permissions permissions;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
    private Collection<User> userCollection;

    public Role()
    {
    }

    public Role(Integer id)
    {
        this.id = id;
    }

    public Role(Integer id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Permissions getPermissions()
    {
        return permissions;
    }

    public void setPermissions(Permissions permissions)
    {
        this.permissions = permissions;
    }

    public Collection<User> getUserCollection()
    {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection)
    {
        this.userCollection = userCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role))
        {
            return false;
        }
        Role other = (Role) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "entityBeans.Role[ id=" + id + " ]";
    }

}
