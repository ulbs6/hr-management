/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import entity.Applicant;
import entity.Job;
import entity.Permissions;
import entity.Role;
import entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cstlm
 */
@Stateless
public class RequestBean implements RequestBeanLocal
{

    @PersistenceContext(unitName = "BusinessRecruit-ejbPU")
    private EntityManager em;

    @Override
    public List<User> getAllUsers()
    {
        return em.createNamedQuery("User.findAll", User.class).getResultList();
    }

    @Override
    public User getUser(int id)
    {
        return em.find(User.class, id);
    }

    @Override
    public void addUser(String firstName, String lastName, String email, String username, String password, String role)
    {
        int roleId = Integer.parseInt(role);
        User newUser = new User();
        newUser.setEmail(email);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setPassword(password);
        newUser.setId(Math.abs(username.hashCode() + email.hashCode()));
        newUser.setRole(em.find(Role.class, roleId));
        newUser.setUsername(username);
        em.persist(newUser);
    }

    @Override
    public void updateUser(String firstName, String lastName, String email, String username, String password, String userId, String role)
    {
        User user = em.find(User.class, Integer.parseInt(userId));
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setRole(em.find(Role.class, Integer.parseInt(role)));
        user.setUsername(username);
        em.merge(user);
    }

    @Override
    public List<Role> getAllRoles()
    {
        return em.createNamedQuery("Role.findAll", Role.class).getResultList();
    }

    @Override
    public void removeUser(int id)
    {
        em.remove(em.find(User.class, id));
    }

    @Override
    public List<Job> getAvailableJobs()
    {
        List<Job> jobsList = em.createNamedQuery("Job.findAllApproved").getResultList();
        return jobsList;
    }

    @Override
    public void createRole(String roleName, boolean addApplicant, boolean addJob, boolean addUser, boolean editApplicant, boolean editJob, boolean editUser, boolean removeApplicant, boolean closeJob, boolean deleteUser)
    {
        Permissions permissions = new Permissions();
        permissions.setId(Math.abs(roleName.hashCode()));
        permissions.setAddApplicant(addApplicant);
        permissions.setAddNewJob(addJob);
        permissions.setAddNewUser(addUser);
        permissions.setCloseJob(closeJob);
        permissions.setDeleteUser(deleteUser);
        permissions.setEditAplicant(editApplicant);
        permissions.setEditJob(editJob);
        permissions.setEditUser(editUser);
        permissions.setRemoveApplicant(removeApplicant);

        Role newRole = new Role();
        newRole.setId(Math.abs(roleName.hashCode()));
        newRole.setName(roleName);
        newRole.setPermissions(permissions);

        try
        {
            em.persist(newRole);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    @Override
    public boolean isValidCredentials(String username, String password)
    {
        try
        {
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
            return user.getPassword().equals(password);

        }
        catch (Exception ex)
        {
            return false;
        }
    }

    @Override
    public void addApplicant(int jobId, String name, String filePath)
    {
        Applicant applicant = new Applicant();
        applicant.setCvFileName(filePath);
        applicant.setName(name);
        applicant.setId(Math.abs(filePath.hashCode()));
    }

    @Override
    public User getUser(String username, String password)
    {
        return em.createNamedQuery("User.findByUsername&Password", User.class)
                .setParameter("username", username)
                .setParameter("password", password)
                .getSingleResult();
    }
}
