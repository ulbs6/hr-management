/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import entity.Job;
import entity.Role;
import entity.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cstlm
 */
@Local
public interface RequestBeanLocal
{

    public List<User> getAllUsers();

    public User getUser(int id);

    public void addUser(String firstName, String lastName, String email, String username, String password, String role);

    public void updateUser(String firstName, String lastName, String email, String username, String password, String userId, String role);

    public List<Role> getAllRoles();

    public void removeUser(int id);

    public List<Job> getAvailableJobs();

    public void createRole(String roleName, boolean addApplicant, boolean addJob, boolean addUser, boolean editApplicant, boolean editJob, boolean editUser, boolean removeApplicant, boolean closeJob, boolean deleteUser);

    public boolean isValidCredentials(String username, String password);
    
    public void addApplicant(int jobId,String name,String filePath);

    public User getUser(String username, String password);

}
