/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import entity.User;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author cstlm
 */
@Singleton
public class UserSessionBean implements UserSessionBeanLocal
{

    @EJB
    private RequestBeanLocal requestBean;

    private static User user;

    @Override
    public void setUserSession(String username, String password)
    {
        user = requestBean.getUser(username, password);
    }

    @Override
    public User getUserSession()
    {
        return user;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
