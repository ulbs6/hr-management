/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package request;

import entity.User;
import javax.ejb.Local;

/**
 *
 * @author cstlm
 */
@Local
public interface UserSessionBeanLocal
{

    void setUserSession(String username, String password);

    User getUserSession();
}
