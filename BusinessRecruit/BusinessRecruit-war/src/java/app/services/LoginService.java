/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.services;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import request.RequestBeanLocal;
import request.UserSessionBeanLocal;

/**
 *
 * @author cstlm
 */
public class LoginService
{

    UserSessionBeanLocal userSessionBean = lookupUserSessionBeanLocal();

    private RequestBeanLocal requestBean = lookupRequestBeanLocal();

    private LoginService()
    {
    }

    public static LoginService getInstance()
    {
        return LoginServiceHolder.INSTANCE;
    }

    private static class LoginServiceHolder
    {

        private static final LoginService INSTANCE = new LoginService();
    }

    public boolean tryLogin(String username, String password)
    {
        if (requestBean.isValidCredentials(username, password))
        {
            userSessionBean.setUserSession(username, password);
            return true;
        }
        return false;
    }

    private RequestBeanLocal lookupRequestBeanLocal()
    {
        try
        {
            Context c = new InitialContext();
            return (RequestBeanLocal) c.lookup("java:global/BusinessRecruit/BusinessRecruit-ejb/RequestBean!request.RequestBeanLocal");
        }
        catch (NamingException ne)
        {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private UserSessionBeanLocal lookupUserSessionBeanLocal()
    {
        try
        {
            Context c = new InitialContext();
            return (UserSessionBeanLocal) c.lookup("java:global/BusinessRecruit/BusinessRecruit-ejb/UserSessionBean!request.UserSessionBeanLocal");
        }
        catch (NamingException ne)
        {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
