/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import request.RequestBeanLocal;

/**
 *
 * @author cstlm
 */
@WebServlet(name = "Role", urlPatterns =
    {
        "/role-form"
})
public class Role extends HttpServlet
{

    @EJB
    private RequestBeanLocal requestBean;

    private static final String ROLE_PAGE = "/WEB-INF/Roles/role-form.jsp";
    private static final String ADMIN_PAGE = "/WEB-INF/Admin/admin.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        request.getRequestDispatcher(ROLE_PAGE).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String roleName = request.getParameter("roleName");
        boolean isAddApplicant = request.getParameter("addApplicant") != null;
        boolean isEditApplicant = request.getParameter("editApplicant") != null;
        boolean isRemoveApplicant = request.getParameter("removeApplicant") != null;
        boolean isAddJob = request.getParameter("addJob") != null;
        boolean isEditJob = request.getParameter("editJob") != null;
        boolean isCloseJob = request.getParameter("closeJob") != null;
        boolean isAddUser = request.getParameter("addUser") != null;
        boolean isEditUser = request.getParameter("editUser") != null;
        boolean isDeleteUser = request.getParameter("deleteUser") != null;

        try
        {
            requestBean.createRole(
                    roleName,
                    isAddApplicant,
                    isAddJob,
                    isAddUser,
                    isEditApplicant,
                    isEditJob,
                    isEditUser,
                    isRemoveApplicant,
                    isCloseJob,
                    isDeleteUser
            );
            response.sendRedirect("./admin");
        }
        catch (Exception ex)
        {
            request.setAttribute("errorMessage", ex);
            request.getRequestDispatcher(ROLE_PAGE).forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
