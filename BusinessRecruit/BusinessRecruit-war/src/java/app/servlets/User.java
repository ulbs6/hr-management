/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import request.RequestBeanLocal;

/**
 *
 * @author cstlm
 */
@WebServlet(name = "User", urlPatterns =
    {
        "/user-form"
})
public class User extends HttpServlet
{

    @EJB
    private RequestBeanLocal requestBean;

    private static final String USER_FORM = "/WEB-INF/User/user-form.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String userId = request.getParameter("userId");
        String action = request.getParameter("action");
        if (userId != null && !userId.isEmpty())
        {
            request.setAttribute("user", requestBean.getUser(Integer.parseInt(userId)));
            if (action != null && action.equals("delete"))
            {
                requestBean.removeUser(Integer.parseInt(userId));
                response.sendRedirect("./admin");
                return;
            }
        }
        request.setAttribute("rolesList", requestBean.getAllRoles());
        request.getRequestDispatcher(USER_FORM).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");
        String userId = request.getParameter("userId");
        String role = request.getParameter("role");

        if (!password.equals(confirmPassword))
        {
            processRequest(request, response);
        }
        else if (userId == null || userId.isEmpty())
        {
            requestBean.addUser(firstName,
                                lastName,
                                email,
                                username,
                                password,
                                role);
        }
        else
        {
            requestBean.updateUser(firstName,
                                   lastName,
                                   email,
                                   username,
                                   password,
                                   userId,
                                   role);
        }
        response.sendRedirect(request.getContextPath() + "/admin");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
