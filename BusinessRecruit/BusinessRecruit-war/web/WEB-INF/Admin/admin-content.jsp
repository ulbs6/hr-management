<%-- 
    Document   : admin-content
    Created on : Jan 21, 2016, 4:36:55 PM
    Author     : cstlm
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix ="fmt" %>

<table class="user-listing">
	<tr>
		<th>Username</th>
		<th>Email</th>
		<th>First Name</th>
                <th>Last Name</th>
                <th>Role</th>
	</tr>
	
        <core:forEach var="user" items="${usersList}" varStatus="status">
            <tr class="${status.index%2==0 ? 'alt' : ''}">
                <td><a href="${pageContext.request.contextPath}/user-form?userId=${user.id}">${user.username}</a></td>
                <td>${user.email}</td>
                <td>
                    ${user.firstName}
                </td>
                <td>
                    ${user.lastName}
                </td>
                <td>${user.role.name}</td>
            </tr>
        </core:forEach>
</table>
<a href="${pageContext.request.contextPath}/user-form">Add new user</a>
<a href="${pageContext.request.contextPath}/role-form">Add new role</a>

