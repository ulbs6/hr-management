<%-- 
    Document   : template
    Created on : Jan 21, 2016, 4:21:30 PM
    Author     : cstlm
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
    <head>
        <title>${param.title}</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/CSS/style.css"/>
    </head>
    <body>
        <jsp:include page="/WEB-INF/Common/header.jsp"/>

        <h2 class="subtitle">${param.title}</h2>

        <jsp:include page="/WEB-INF/${param.content}.jsp"/>

        <jsp:include page="/WEB-INF/Common/footer.jsp"/>

    </body>
</html>
