<%-- 
    Document   : home-content
    Created on : Jan 23, 2016, 1:19:17 PM
    Author     : Vlad Orasan
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="core" %>


<div id="wrapper">

    <div id="login-form">
        <a href="./login">Authentication</a>
        <input name="signup" type="button" value="Sign up">
    </div>

    <div id="available-jobs">
        <h1>Available jobs</h1>
        <table border="1">
            <tr>
                <th>Name</th>
                <th>Benefits</th>
                <th>Requirements</th>
                <th>Required employees</th>
                <th>Apply</th>
            </tr>
            <core:forEach var="job" items="${jobsList}" varStatus="status">
                <tr>
                    <td>${job.name}</td>
                    <td>${job.benefits}</td>
                    <td>${job.requirements}</td>
                    <td>${job.requiredEmployees}</td>
                    <td><a href="./jobApply">Apply</a></td>
                </tr>
            </core:forEach>
        </table>
    </div>

</div>

