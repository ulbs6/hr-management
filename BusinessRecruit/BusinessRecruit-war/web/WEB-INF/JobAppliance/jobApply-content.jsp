<%-- 
    Document   : jobApply-content
    Created on : Jan 23, 2016, 10:21:43 PM
    Author     : Vlad Orasan
--%>

<div id="wrapper">
    
    <div id="apply-form">
        <form name="jobApplyForm" action="./jobApply" method="POST">
            <table border="1">
                <tr>
                    <td> <label> Full name </label> </td>
                    <td> <input type="text" name="name" ></td>
                </tr>
                <tr>
                    <td> <label> Upload CV </label> </td>
                    <td> <input type="file" name="cvToUpload"> </td>
                </tr>
            </table>
            <input name="apply" type="button" value="Apply">
        </form>
    </div>
    
</div>

