<%-- 
    Document   : role-form-content
    Created on : Jan 24, 2016, 12:27:24 PM
    Author     : cstlm
--%>

<form method="POST" action="${pageContext.request.contextPath}/role-form">
    <fieldset>
        <legend>
            Add new role
        </legend>
            <h5>Role Information</h5>

            <div>
                <label>Name:</label>
                <input type="text" name="roleName" id="roleNameInput" />
            </div>
            
            <h5>Permissions</h5>
            
            <div>
                <label>Add applicant</label>
                <input type="checkbox" name="addApplicant" id="addApplicantInput" />
            </div>
            
            <div>
                <label>Edit applicant</label>
                <input type="checkbox" name="editApplicant" id="editApplicantInput" />
            </div>
            
            <div>
                <label>Remove applicant</label>
                <input type="checkbox" name="removeApplicant" id="removeApplicantInput" />
            </div>
            
            <div>
                <label>Add job</label>
                <input type="checkbox" name="addJob" id="addJobInput" />
            </div>
            
            <div>
                <label>Edit job</label>
                <input type="checkbox" name="editJob" id="editJobInput" />
            </div>
            
            <div>
                <label>Close job</label>
                <input type="checkbox" name="closeJob" id="closeJobInput" />
            </div>
            
            <div>
                <label>Add user</label>
                <input type="checkbox" name="addUser" id="addUserInput" />
            </div>
            
            <div>
                <label>Edit user</label>
                <input type="checkbox" name="editUser" id="editUserInput" />
            </div>
            
            <div>
                <label>Delete user</label>
                <input type="checkbox" name="deleteUser" id="deleteUserInput" />
            </div>

            <div class="button-row">
                <a href="${pageContext.request.contextPath}/admin">Cancel</a> or
                <input type="submit" value="Submit" />
            </div>
                
                <div>
                    <label class="error-msg">${errorMessage}</label>
                </div>
    </fieldset>
    
</form>

