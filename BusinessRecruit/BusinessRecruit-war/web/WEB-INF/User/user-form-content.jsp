<%-- 
    Document   : user-content
    Created on : Jan 22, 2016, 7:48:39 PM
    Author     : cstlm
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<form class="semantic" method="post"
      action="${pageContext.request.contextPath}/user-form">
    <fieldset>
        <legend>
            <core:choose>
                <core:when test="${not empty user.id }">
                    Updating User
                </core:when>
                <core:otherwise>
                    Adding User
                </core:otherwise>
            </core:choose>
        </legend>

        <div>
            <label for="firstName">First name:</label> <input type="text" name="firstName"
                                                              id="firstNameInput" value="${user.firstName}" />
        </div>

        <div>
            <label for="lastName">Last name:</label> <input type="text" name="lastName"
                                                            id="lastNameInput" value="${user.lastName}" />
        </div>

        <div>
            <label for="username">Username:</label> <input type="text" name="username"
                                                           id="usernameInput" value="${user.username}" />
        </div>

        <div>
            <label for="email">Email:</label> <input type="email" name="email"
                                                     id="emailInput" value="${user.email}" />
        </div>

        <div>
            <label for="password">Password:</label> <input type="password" name="password"
                                                           id="passwordInput" value="${user.password}" />
        </div>

        <div>
            <label for="confirm-password">Confirm password:</label> <input type="password" name="confirmPassword"
                                                                           id="confirmPasswordInput" value="${user.password}" />
        </div>

        <select name="role">
            <option value="">Choose a role</option>
            <core:forEach var="role" items="${rolesList}" >
                <option value="${role.id}">${role.name}</option>  
            </core:forEach>
        </select>
        <core:if test="${not empty user.id}">
            <input type="hidden" name="userId" value="${user.id}" />
        </core:if>

        <div class="button-row">
            <a href="${pageContext.request.contextPath}/admin">Cancel</a>  or <input type="submit" value="Submit" />
        </div>

        <core:if test="${not empty user.id}">
            <div class="button-row">
                <a href="./user-form?action=delete&userId=${user.id}">Delete user</a>
            </div>
        </core:if>
    </fieldset>
</form>
